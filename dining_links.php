<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals by Owner :: Sunriver Dining &amp; Food</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Sunriver Dining &amp; Food</strong></h2>
	<ul class="list_002">
		<li><strong>Base Camp Grill:</strong> Burgers / Sandwiches / Soups / Salads / 541-598-8868</li>
		<li><strong>Bellatazza Cafe:</strong> Specialty Coffee / Tea / Pastries / Sandwiches / Salads / 541-593-4999</li>
		<li><strong>Blondies:</strong> Pizza / Pasta / Sandwiches / Salads / Coffee / 541-593-1019</li>
		<li><strong>Boondocks:</strong> Steaks / Burgers / Seafood / Full Bar / 541-593-2275</li>
		<li><strong>Bottom's Up:</strong>  Burgers / Sandwiches / Full Bar / 541-536-7994</li>
		<li><strong>Country Store:</strong> Deli Sandwiches / 541-593-8113</li>
		<li><strong>Cafe Sintra:</strong> Breakfast / Lunch / Sandwiches / Soups / Coffee 541-593-1222</li>
		<li><strong>El Caporal:</strong> Traditional Mexican Menu / 541-593-3335</li>
		<li><strong>Goodys:</strong> Homemade Ice Cream / Candy / Kettle Corn / 541-593-2155</li>
		<li><strong>Hola! Mexican:</strong> Peruvian Menu / On The River / 541-593-8880</li>
		<li><strong>Hot Lava Baking & Coffee:</strong> Pastries / Donuts / Breads / Deli Sandwiches / Coffee / 541-593-3986</li>
		<li><strong>Marcello's:</strong> Italian Menu / Pizza / Full Bar / 541-593-8300</li>
		<li><strong>Marketplace:</strong> Deli Sandwiches / 541-593-8166</li>
		<li><strong>Meadows Dining Room:</strong> At Sunriver Lodge / Steaks / Seafood / Pasta / Salads / 541-593-1000</li>
		<li><strong>Merchant Trader:</strong> At Sunriver Lodge / Sandwiches / Soups / Salads / Burgers / 541-593-1000</li>
		<li><strong>Owl's Nest:</strong> Pub Fare / Full Bar / 541-593-3730</li>
		<li><strong>Pellerito's Pizza:</strong>  Pizza / Sandwiches / Salads / 541-593-8111</li>
		<li><strong>South Bend Bistro:</strong> Pacific Northwest Menu / 541-593-3881</li>
		<li><strong>Village Bar &amp; Grill:</strong> Burgers / Chili / Sandwiches / Salads / Full Bar / 541-593-1100</li>
		<li><strong>Villagio Expresso:</strong> Drive Thru Specialty Coffes / 541-593-5042</li>
	</ul>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
