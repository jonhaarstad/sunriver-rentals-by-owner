<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals by Owner :: Rates</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>FREQUENTLY ASKED QUESTIONS</strong></h2>
	<h4><strong>Is a deposit required to make a reservation?</strong></h4>
		<p>Yes. A one night deposit is required to secure a reservation.  If booking 5 nights or more, a two night deposit is required.  The balance is not due until 30 days before check-in. Once final payment is received, check-in information will be sent. We accept Visa, MasterCard, or Check.</p>
	<h4><strong>Where do I pick up/drop off keys? </strong></h4>
		<p>You will be emailed a 4 digit code to retrieve the key from the lockbox located just to the right of the front door.</p>
	<h4><strong>What time is check in/check out? </strong></h4>
		<p>Check in is at 4:00 PM Check out is at 11:00 am. Check in time may be delayed during peak season dates.</p>
    <h4><strong>How old do I have to be to rent one of your homes?</strong></h4>
    	<p>Contracted guest must be at least 25 years of age. Subletting of rental home is prohibited.</p>
    <h4>What about housekeeping service? </h4>
    	<p>Sunriver Rentals By Owner cleans prior to your arrival and again after you leave.  The Hot Tub is also cleaned following every departure and prior to every check in.</p>
    <h4>What is included with the home?</h4>
    	<p>Kitchen cooking utensils, dishes &amp; silverware, coffee maker, linens and towels are provided.  Washer &amp; Dryer is provided.  High chair &amp; booster chair provided.  Board Games, Video Games, Family Movies also provided. An ample starter supply of products are provided in the home such as toilet paper, paper towels, tissue, laundry detergent, dishwashing detergent, garbage bags, etc. Any additional amounts need to be provided by guest. </p>
    <h4>What is your cancellation policy?</h4>
    	<p>If for any reason you have to cancel your reservation, your Reservation Deposit would be forfeited, and a refund of any other funds received, would be based on the number of nights we are able to re-book.  No refunds will be given for delayed arrivals or early departures.</p>
		<!-- <p>There is a $100 charge on all cancellations. If cancellation occurs less than 60 days prior to arrival date, the full deposit will be forfeited. If less than 30 days, full rental amount will be charged. No refunds will be given for delayed arrivals or early departures.</p> -->
	<h4>Where do I park my RV/Boat/Trailer?</h4>
		<p>These vehicles must be parked in the owner storage area located near circle 4. The cost is $8.00 per night with a refundable $20.00 key deposit. Renter must arrange space via the SROA during regular hours of business 8:00 am - 5:00 PM Monday through Friday. The phone number is 541) 593-2411. Please direct all questions regarding this matter to the SROA.</p>
	<h4>What if we lose a personal item?</h4>
		<p>Sunriver Rentals By Owner is not responsible for any lost or stolen items, however if you leave items behind you will be responsible for any costs involved in returning that item to you plus a $25 service fee.</p>
	<h4>What if something in the home doesn't work or is broken?</h4>
		<p>Phone numbers will be posted in the home for emergency contacts (housekeeping, maintenance, etc.) . While we can't guarantee equipment function of 100%, repairs to malfunctioning equipment will be done as soon as possible. We will attempt to fix any problems in a timely manner and with in our means.</p>
	<h4>Do you provide SHARC Passes or North Pool Passes?  How about Tennis Passes?</h4>
		<p>Yes. We provide "Unlimited Use" Passes for SHARC, North Pool, and all Tennis Courts.</p>
	<!--
<h4>Do you provide pool passes?</h4>
		<p>Yes.  We provide FREE pool and tennis passes.</p>
-->
	<h4>What if we lose a pool pass?</h4>
		<p>There is a $75.00 replacement charge for lost passes.</p>
	<h4>Can we bring our pet?</h4>
		<p>No.  Absolutely no pets are allowed in our homes or on the premises! There is a $250 fine for any violation of this rule, along with forfeiture of the home without refund.</p>
	<h4>Do the homes have cable TV?</h4>
		<p>Yes!</p>
	<h4>Are there phones in the homes?</h4>
		<p>Phones are available in all of our homes. Local calls are free.</p>
	<h4>Is high speed Internet available?</h4>
		<p>We do have free wireless high speed Internet in all of our homes. </p>
	<h4>Is there a BBQ?</h4>
		<p>Yes! Gas BBQ's, along with propane is provided.  Charcoal BBQ's are not permitted in Sunriver due to the threat of fire.</p>
	<h4>Are there bikes included with the rental?</h4>
		<p>Yes.  Number of bikes varies with size of home.  Additional bikes can be rented nearby.</p>
	<h4>What is there to do in Sunriver?</h4>
		<p>Sunriver is a resort community approximately 3300+ acres. Sunriver has two 18 hole championship golf courses, 29 outdoor tennis court, two Olympic size pools, Marina/canoeing, riding stables, 35+ miles of paved bike paths, nature center, observatory, bird watching, white water rafting, restaurants, shopping. The Deschutes River boarders the West side of Sunriver for 5+ miles, surrounded by Deschutes National Forest and only 18 miles from Mt. Bachelor. Other activities within an hour or two (or less) of Sunriver include the High Desert Museum, Lava River Caves, Lava Butte, Lava Cast Forest, Crater Lake, Smith Rock and More!   Bend, Oregon is only 15 minutes away with incredible restaurants and shopping.</p>
<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
