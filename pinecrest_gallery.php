<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pinecrest Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Pinecrest Lodge Photo Gallery </strong></h2>
	<table width="425" cellpadding="3" cellspacing="0" border="0">
    	<tr>
        	<td colspan="2"><object width="480" height="385" border="3" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/FkBuZY7f18c02481fb223ad901c606f278a182pWeGW/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":true}' /></object></td>
            <td rowspan="19" valign="top"><h2><strong>FLOOR PLANS</strong></h2>
              <h3>DOWNSTAIRS</h3>
            <p><img src="i/pinecrest/floorplan_downstairs.gif" alt="Downstairs Floor Plan" width="300" height="564" /></p>
            <h3>UPSTAIRS</h3>
            <p><img src="i/pinecrest/floorplan_upstairs.gif" alt="Upstairs Floor Plan" width="300" height="570" /></p></td>
        </tr>
		<tr>
			<td align="center" width="206"><img src="i/backwoods1/LivingRoom_01_0208.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
			<td align="center" width="206"><img src="i/backwoods1/LivingRoom_01_1209.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Great Room - 40&quot; Plasma TV</td>
			<td align="center">Great Room</td>
	  </tr>
		<tr>
          <td align="center"><img src="i/backwoods1/LivingRoom_04_0208.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/LivingRoom_03_0208.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Great Room - Huge View Windows</td>
		  <td align="center">Great Room from Above</td>
	  </tr>
        <tr valign="top">
		  <td align="center"><img src="i/pinecrest/dining_room.jpg" alt="Dining Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/pinecrest/pinecrest_kitchen.jpg" alt="Kitchen" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Dining Room - Seats up to 10</td>
          <td align="center">Fully Equipped Kitchen</td>
	  </tr>

		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/BreakfastBar_1209.jpg" alt="Free High-Speed Internet" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/2009_12_DoubleSinks.jpg" alt="Bath with Tile Counters &amp; Double Sinks" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Breakfast Bar / Free Wireless Internet</td>
          <td align="center">Both Baths Have Double Sinks</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/master_bedroom_01_0208.jpg" alt="Master Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/master_bdr_2view.jpg" alt="Master Bedroom with Sitting Area" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center"> King Master Suite</td>
		  <td align="center">King Master Suite Sitting Area</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/0726_bathroom1.jpg" alt="Master Bath" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/2009_12_Bedroom2.jpg" alt="Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Master Suite Private Bath</td>
		  <td align="center">Bedroom #2 - King Bed</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/2009_12_Bedroom3.jpg" alt="Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/bed_0106.jpg" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Twin Bedroom / TV / Video Games</td>
		  <td align="center">Large Loft / Game Table</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/2009_12_HotTub.jpg" alt="Private Hot Tub" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/2009_12_LaundryRoom.jpg" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Private Hot Tub </td>
		  <td align="center">Laundry Room</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods1/2009_12_Deck.jpg" alt="View from Back Deck" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods1/backdeck_web.jpg" alt="View from Back Deck" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Large Wrap-Around Deck</td>
		  <td align="center">View from Back Deck</td>
	  </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="pinecrest.php">Back to Pinecrest Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
