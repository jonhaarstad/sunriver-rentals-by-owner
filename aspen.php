<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Aspen Meadow Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
    <h3>Rooms (sleeps 12):</h3>
    <ul>
      	<li> Master Suite #1 - King Bed </li>
      	<li>Master Suite #2 - King Bed</li>
      	<li>Bedroom #3 - Queen Bed </li>
      	<li>Bedroom #4 - Twin Bunks / Twin Trundle</li>
      	<li>Great Room - Queen Hide-a-Bed </li>
        <li>Extra Bed - Twin Roll-a-Way</li>
    </ul>
    <h3>Special Features:</h3>
    <ul>
		<li>Private Hot Tub </li>
		<li>Air Conditioning</li>
		<li>Free High Speed Wireless Internet</li>
	  	<li>10 SHARC &amp; Tennis Passes</li>
		<li>Fully Equipped Kitchen </li>
		<li>Slab Granite Counters </li>
	  	<li>Hickory Cabinets </li>
     	<li>42&quot; Plasma TV in Living Room</li>
     	<li>TV/DVD in Every Room </li>
      	<li>Stereo CD System / CDs </li>
      	<li>Family Movies </li>
      	<li>Video Games </li>
      	<li>Microwave</li>
      	<li>Refrigerator</li>
       	<li>Dishwasher</li>
       	<li>Washer and Dryer</li>
      	<li>Deck with Gas Barbecue</li>
      	<li>2 Car Garage</li>
       	<li>Ping Pong Table</li>
       	<li>6  Bikes</li>
   	  <li>Family Board Games</li>
		<li>Books</li>
		<li>All Linens Provided</li>
 	</ul>
  </div>
  <h2><strong>Aspen Meadow  Lodge </strong></h2>
	<p><strong><img src="i/aspen/Exterior.jpg" alt="Exterior Picture of Aspen Meadow Lodge" width="250" height="188" class="imgLeft" />WELCOME TO ASPEN MEADOW LODGE. This brand new 2300 square foot lodge-style vacation home is the perfect place for your next Sunriver get-a-way.</strong> The Expansive Great Room, High Vaulted Ceilings, Large Windows, Rock Fireplace, Gourmet Kitchen, Slab Granite Counters, Hickory Cabinets, Large Plasma TV,and Private Hot Tub, are just a few of the distinctive features that set this home apart. From the moment you walk through the front door, you know that this is going to be an exceptional vacation experience. </p>
	<p>Aspen Meadow Lodge includes beautiful furnishings throughout, fully furnished kitchen with everything the gourmet cook desires, 4 Bedrooms, 3 Baths, 2 Master Suites, Soaker Tub, Double Sinks, Walk In Closet, TV's &amp; DVD players in every room, Video Games, Family Movies, Board Games, Ping Pong Table, Bikes, Toys for the kids, and so much more. This home just received a 5 Star Rating (the highest possible) for its exceptional quality.</p>
	<p>You will love the location of this beautiful home. Located at #6 Crag, it is near Fort Rock Park and just a short bike ride to the Sunriver Village Mall, Sunriver Pool, or the Beautiful Sunriver Lodge. This home is great for family reunions, golf groups, ski groups, business retreats, church groups and more! </p>
	<p><img src="i/aspen/KitchenDiningFromAbove.jpg" alt="Kitchen View" width="250" height="188" class="imgRight" />We also own 4 other high quality vacation homes in Sunriver. To view all of these homes along with availability calendars and seasonal specials, click the &quot;All My Rental Listings&quot; link below. </p>
  <p>SUNRIVER has three championship golf courses, 29 tennis courts, 30 miles of paved bicycle paths, two large swimming pools, riding stables, white-water rafting, canoeing, fishing in the famous Deschutes River, and so much more! Sunriver is located only 15 minutes from Mount Bachelor, one of the top ski destinations in America.  </p>
	<p><strong>No Smoking / No Pets</strong></p>
	<p><!-- &raquo; <a href="specials.php">See SPECIALS!</a><br /> -->
	  &raquo; <strong><a href="aspen_gallery.php">Photo Gallery</a></strong><br />
	  &raquo; <a href="http://www.vrbo.com/18658#calendar" target="_blank"> Availability Calendar </a><br />
      &raquo; <strong><a href="http://www.vrbo.com/18658#rates" target="_blank">Rates</a></strong><br />
  	  &raquo; <a href="reservations.php">Make a Reservation</a></p>	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
<!--
	<h3><strong>Aspen Meadows Lodge Rental Rates </strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$275 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$295 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$350 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$450 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$165</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 10 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
	<p>&nbsp;</p>
    <object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/qNgWVo7f18c02481fb223ad901c606f278a1826AbXU/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":false}' /></object>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
