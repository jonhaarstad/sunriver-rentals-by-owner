<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals By Owner Welcomes You</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
<div id="specials"><a href="/rates.php"><img src="i/2015-07_specials.png" border="0" /></a></div>
	<h1 style="font-size:18px; color:green; position:relative;top:-7px;margin-bottom:20px;padding-bottom:0;">New SHARC Water Park and Sledding Facility Now Open!</h1>
	<h2><strong>Looking for a mountain retreat? </strong></h2>
	<p><strong><img src="i/conifer/living_homePg.jpg" alt="Sunriver Rental by Owner Home" width="175" height="194" class="imgLeft" />Welcome to Sunriver Rentals By Owner.  Whether you&rsquo;re looking for a spacious 3 Bedroom Home or a magnificent 7 Bedroom Lodge, or anything in between, we have something that will be just right for your family or group. By renting directly through us as owners, your savings will be considerable. Be sure to check out our specials page.</strong></p>
	<p><strong>&raquo; <a href="specials.php">See SPECIALS! </a></strong></p>
	<p><strong><img src="i/wifi-logo.gif" width="75" height="45" class="imgRightNB" />All of our homes have FREE High Speed Wireless Internet.</strong></p>
	<p>All of our homes are smoke-free. </p>
	<p><strong>SUNRIVER</strong> is listed as the<strong> #1 destination resort in Oregon</strong>. It has 3 championship golf courses, 29 tennis courts, 30 miles of paved bicycle paths, 2 large swimming pools, riding stables, white-water rafting, canoeing, fishing in the famous Deschutes River, and so much more! Sunriver is located only 15 minutes from Mount Bachelor, one of the top ski destinations in America.</p>
	<h3><strong>FOUR Beautiful homes to choose from:</strong></h3>
	<br />
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
		  <td valign="top" nowrap="nowrap" class="image"><p><a href="conifer.php"><img src="i/0906_conifer_home_ad.jpg" alt="Conifer Lodge Rental" width="200" height="170" class="imgLeftTable" /></a></p>
		      <h1>#13 Conifer</h1>
	        <p><a href="conifer_gallery.php" target="_self"><img src="i/video_link.gif" width="50" height="65" border="0" class="imgRightNB" /></a><strong>7 Bedrooms</strong> <br />
		        5 Baths<br />
				Sleeps 20<br />
				SHARC Passes</p>
          </td>
		  <td valign="top" nowrap="nowrap" class="image"><p><a href="cascade.php"><img src="i/0307_cascade_home_ad.jpg" alt="Cascade Lodge Rental" width="200" height="170" border="0" class="imgLeftTable" /></a></p>
		    <h1>#17 Shagbark</h1>
		    <p><a href="cascade_gallery.php" target="_self"><img src="i/video_link.gif" border="0" class="imgRightNB" /></a><strong>7 Bedrooms</strong> <br />
		      5 Baths<br />
	        Sleeps 18<br />
				SHARC Passes</p>
	      </td>
		</tr>
		<tr>
		  <td class="home"><p><strong>Special Features Include:</strong> Air Conditioning / Huge Big Screen TV / Gourmet Kitchen with Island Bar / Double-Sized Hot Tub / Great Location Near Fort Rock Park / Beautiful Log Accents Throughout / River Rock Fireplace in Great Room / 3 Master Suites (2 with Slate Fireplaces) / High-Speed Internet Access / TV in Every Room / 6 New Cruiser Bikes / Ping Pong / Video Games / Double Car Garage / Much More! </p>
          <p>[ <a href="conifer.php"> Pricing, Availability &amp; Photo/Video Gallery</a> ]</p></td>
		  <td class="home">
		    <p><strong>Special Features Include:</strong> Air Conditioning / Hot Tub / Free Wireless High-Speed Internet, 16 Unlimited Pool/Tennis Passes for Summer Rentals, Pool Table, Big Screen TV in Game Room, Lots of Family DVD Movies, Sony Playstation 2 with Video Games, Board Games, Books &amp; Magazines, Two Dining Areas (seats up to 18), River Rock Fireplace in Great Room, Beautiful Log Accents Throughout &amp; Much More! </p>
		    <p>[ <a href="cascade.php"> Pricing, Availability &amp; Photo/Video Gallery </a> ]</p></td>
		</tr>
		<tr>
		  <td valign="top" nowrap="nowrap" class="image">
				<p><a href="evergreen.php"><img src="i/0906_evergreen_home_ad.jpg" alt="Evergreen Lodge Rental" width="200" height="170" border="0" class="imgLeftTable" /></a></p>
		    <h1>#5 Backwoods</h1>
		    <p><a href="evergreen_gallery.php" target="_self"><img src="i/video_link.gif" width="50" height="65" border="0" class="imgRightNB" /></a><strong>5 Bedrooms</strong><br />
		        Bonus Room<br />
		        4 Baths <br />
	        	Sleeps 14<br />
						SHARC Passes</p>
      </td>
			<td valign="top" nowrap="nowrap" class="image">
				<p><a href="pinecrest.php"><img src="i/0906_pinecrest_home_ad.jpg" alt="Pinecrest Lodge Rental" width="200" height="170" border="0" class="imgLeftTable" /></a></p>
		    <h1>#1 Backwoods</h1>
		    <p><a href="pinecrest_gallery.php" target="_self"><img src="i/video_link.gif" width="50" height="65" border="0" class="imgRightNB" /></a><strong>3 Bedrooms<br />
		      </strong> Large Loft <br />
		      2 Baths<br />
		      Sleeps 10<br />
					SHARC Passes</p>
		  </td>
		</tr>
		<tr>
		  <td class="home"><p><strong>Special Features Include:</strong> Best Location In Sunriver / Just across from Village Mall / Air Conditioning / Brand New Kitchen with Island Bar / Large Deck with Hot Tub / Bonus Room With Huge Big Screen TV / River Rock Fireplace in Great Room / 2 Master Suites with Private Baths / High-Speed Internet Access / TV in Every Room / 5 New Cruiser Bikes / Video Games / Covered Porch / Double Car Garage / Much More! </p>
		      <p>[ <a href="evergreen.php">Pricing, Availability &amp; Photo/Video Gallery</a> ]</p></td>
			<td class="home"><p><strong>Special Features Include:</strong> Great Location / Across from Village Mall / Air Conditioning / Beautiful Furnishings / Large Deck with Private Hot Tub / Large Loft with Game Table / 17&rsquo; Rock Fireplace in Great Room / Wood Accents Throughout / Master Suite with Private Bath / High-Speed Internet Access / TV in Every Room / Free Pool &amp; Tennis Passes / 5 Bikes / Video Games / 50 Family Movies / Board Games / &nbsp;Much More! </p>
		      <p>[ <a href="pinecrest.php">Pricing, Availability &amp; Photo/Video Gallery </a> ]</p></td>
	  </tr>
  </table>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
