<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Conifer Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
	<h3>Sleeps 20:</h3>
	<ul>
	  	<li> Master Suite #1 - King Bed</li>
      	<li>Master Suite #2 - King Bed</li>
   	  <li>Master Suite #3 - Queen Bed</li>
      	<li>Bedroom #4 - King Bed</li>
      	<li>Bedroom #5 - 2 Twin Beds</li>
      	<li>Bedroom #6 - Double / Twin Bunk / Twin Trundle</li>
      	<li> Bedroom #7 - Queen Sleeper-Sofa / Twin Bunks</li>
	  <li>Great Room - Queen Sleeper-Sofa</li>
  	</ul>
  	<h3>Special Features:</h3>
	<ul>
		<li>Air Conditioning</li>
		<li>Free High Speed Wireless Internet</li>
	  	<li>16 SHARC &amp; Tennis Passes</li>
      	<li>Huge Big Screen TV</li>
      	<li>Double-Sized Hot Tub</li>
      	<li>Custom Log Dining Room Table</li>
      	<li>River Rock Fireplace in Great Room</li>
      	<li>Beautiful Log Accents Throughout</li>
      	<li>3 Master Suites (2 with Slate Fireplaces)</li>
      	<li>TV in Every Room</li>
   	  <li>Ping Pong</li>
      	<li>Video Games - Nintendo Game Cube</li>
      	<li>Custom Chess &amp; Checker Table</li>
      	<li>8  Bikes</li>
   	  <li>Large Corner Lot</li>
      	<li>Double Car Garage</li>
      	<li>Great Location Near Fort Rock Park</li>
      	<li>8 TVs / 6 DVDs / 4 VCRs</li>
      	<li>Stereo CD System / CDs</li>
      	<li>Family Movies</li>
      	<li>Soaking Tub In Master Suite</li>
      	<li>Custom Log Accents Throughout</li>
      	<li>Gourmet Kitchen with Island Bar</li>
      	<li>All Stainless Steel Appliances</li>
      	<li>Large Refrigerator</li>
      	<li>Double Ovens</li>
      	<li>Microwave</li>
      	<li>Washer &amp; Dryer</li>
      	<li>Large Pantry</li>
      	<li>Gas Bar-B-Q Grill</li>
      	<li>All Plush Linens Provided</li>
      	<li>All Cooking Utensils &amp; Dishes Provided</li>
  	</ul>
  </div>
	<h2><strong>Conifer Lodge </strong> </h2>
	<p><img src="i/conifer_outside.jpg" width="175" height="211" class="imgLeft" /><strong>WELCOME TO CONIFER LODGE!</strong> This beautiful brand new 3600-square foot custom lodge-style vacation home is the perfect place for your next Sunriver get-a-way. It features log accents throughout, slate fireplaces, beautiful interior design and furnishings (by Mountain Comfort), spectacular kitchen with island/bar, double oven, all stainless appliances, large pantry, custom log dining room table, double-sized Hot Tub, Big Screen TV, Game Room with  Video Games, Ping Pong Table, Bikes, and so much more! This is a premium 5 star vacation home!</p>
  <p><strong>Seven magnificent bedrooms</strong>, including three Master Suites (two with fireplaces), and five spacious bathrooms. Great Room has a gorgeous river rock fireplace and huge big screen TV. This house was designed with large groups in mind. Great for family reunions, couple groups, professional retreats, Golf groups, conferences, or multiple family vacations. Location is ideal...near <strong><img src="i/conifer/kitchen1_sm.jpg" width="200" height="150" class="imgRight" /></strong>Fort Rock Park (Playground, Volleyball, Horseshoes, Basketball, Softball Fields, Tennis Courts, Covered Pavilion Area with picnic tables and barbecue), large quiet corner lot, and just a short bike ride to the Village Mall or Sunriver Lodge.</p>
	<p><strong>No Smoking / No Pets</strong></p>
	<p><!-- &raquo; <a href="specials.php">See SPECIALS!</a><br /> -->
    &raquo; <a href="conifer_gallery.php">Video &amp; Photo Gallery</a> <br />
    &raquo; <a href="http://www.vrbo.com/34625#calendar" target="_blank">Availability Calendar</a><br /> 
  	&raquo; <a href="http://www.vrbo.com/34625#rates" target="_blank">Rates</a><br />
  	&raquo; <a href="reservations.php">Make a Reservation</a></p>
	<p><strong><img src="i/conifer/living16.jpg" width="175" height="233" class="imgLeft" />Amenities</strong>: Fireplace, Phone, Cable TV, 8TVs, 6DVDs, 4VCRs, Stereo CD Player,
	  Full Kitchen, Microwave, Dishwasher, Refrigerator, Ice Maker, Cooking Utensils
	  provided, Linens provided, Washer &amp; Dryer, Private Oversized Hot Tub,
	  Garage, Gas Grill (BBQ), Bikes, Video Games, Ping Pong. No Smoking,
	  No Pets. </p>
  <p><strong>Activities (on site or nearby):</strong> 2 Swimming Pools, 31 Tennis Courts, 30 Miles
	  of Paved Bike / Walking Paths, 2 Championship Golf Courses, Sage Springs Day
	  Spa, Horseback Riding, Hiking, White Water Rafting, Rock Climbing, Biking,
	  Golf, Racquetball, Basketball, Fitness Center, Horseshoes, Miniature Golf,
	  Fishing, Hunting, Wildlife Viewing, Shopping, Restaurants, Museums, Sightseeing,
	  Swimming, Boating, Water-skiing, Jet Skiing, Canoeing, Downhill Skiing, Cross
	  Country Skiing, Snowboarding, Ice Skating, Snowmobiling, Sledding, ATVs, Mopeds.</p>
	<p>&raquo; View <a href="http://www.rentors.org/guestbook.cfm?pid=36067">comments</a> from previous guests.</p>
<!--
	<h3><strong>Conifer Lodge Rental Rates</strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$595 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$625 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$750 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$850 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$225</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing" valign="top"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 16 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
    <p>&nbsp;</p>
    <object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/x_YSZc7f18c02481fb223ad901c606f278a182QOA76/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":false}' /></object>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
