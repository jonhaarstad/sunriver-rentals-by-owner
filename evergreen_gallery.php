<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Evergreen Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Evergreen Lodge  Video &amp; Photo Gallery </strong></h2>
<table width="425" cellpadding="3" cellspacing="0" border="0">
	<tr>
    	<td colspan="2">
        <object width="480" height="385" border="3" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/pflt247f18c02481fb223ad901c606f278a182d5kMS/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":true}' /></object>
        </td>
            <td rowspan="17" valign="top"><h2><strong>FLOOR PLANS</strong></h2>
              <h3>DOWNSTAIRS</h3>
              <p><img src="i/evergreen/floor_plan.gif" alt="Evergreen Floor Plan" width="300" height="350" /></p>
              <h3>UPSTAIRS</h3>
              <p><img src="i/evergreen/floor_plan_dwn.gif" alt="Floor Plan Downstairs" width="300" height="247" /></p>
          <p>&nbsp;</p></td>
    </tr>
		<tr>
			<td align="center" width="206"><img src="i/evergreen/2009_12_Outside.jpg" alt="Craftsman Exterior" width="250" height="188" border="1" /></td>
		  <td align="center" width="206"><img src="i/backwoods5/inside_01_1227.jpg" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Craftsman Exterior</td>
			<td align="center">Kitchen to Living Room View</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods5/dining1_new.jpg" alt="Kitchen Dining Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/evergreen/kitchen.jpg" alt="Kitchen" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Kitchen Dining Room</td>
		  <td align="center">Kitchen Island</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/evergreen/kitchen_2.jpg" alt="Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/evergreen/hardwoodFloors.jpg" alt="Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Kitchen - All Stainless Appliances</td>
		  <td align="center">New Hardwood Floors</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods5/dining_01_1227.jpg" alt="Dining Room Table" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods5/living_01_1227.jpg" alt="Living Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Dining Area / View Windows</td>
		  <td align="center">Living Room / Fireplace</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/evergreen/2009_12_LivingRoom01.jpg" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods5/masterbed1_new.jpg" alt="King Master Suite #1" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">Living Room / Rock Fireplace / TV/DVD</td>
          <td align="center">King Master Suite #1</td>
    </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods5/bedroom1_new.jpg" alt="Master Bedroom 2" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods5/bedroom2_new.jpg" alt="Queen Bedroom" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">King Master Suite #2</td>
          <td align="center">Queen Bedroom / Vaulted Ceilings</td>
    </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods5/bedroom3_new.jpg" alt="Twin Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods5/bonusroom_01_0208.jpg" alt="Bonus Room" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">Twin Bedroom </td>
          <td align="center">Bonus Room / TV / Video Games</td>
    </tr>
		<tr valign="top">
		  <td align="center"><img src="i/backwoods5/hottub1_new.jpg" alt="Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/evergreen/2009_12_BackDeck.jpg" alt="Back Deck" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">Private Hot Tub</td>
          <td align="center">Back Deck / Gas Grill</td>
    </tr>
		<tr valign="top">
		  <td align="center"><img src="i/evergreen/2009_12_Garage.jpg" alt="Deck View" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/backwoods5/deck_01_1227.jpg" alt="Deck View" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">2 Car Garage / Ping Pong Table / 6  Bikes</td>
          <td align="center">View from Back Deck</td>
	  </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="evergreen.php">Back to Evergreen Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
