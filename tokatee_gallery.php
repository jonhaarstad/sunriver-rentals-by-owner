<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Tokatee Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Tokatee Lodge Video &amp; Photo Gallery</strong></h2>
<table width="425" cellpadding="3" cellspacing="0" border="0">
		<tr>
			<td align="center" width="206"><img src="i/tokatee/greatroom_02.jpg" alt="Large Great Room / Stone Fireplace" width="250" height="188" border="1" /></td>
			<td align="center" width="206"><img src="i/tokatee/dining_01.jpg" alt="Tokatee Living Room" width="250" height="188" border="1" /></td>
		</tr>
<tr valign="top">
			<td align="center">Large Great Room / Stone Fireplace</td>
			<td align="center">Custom Dining Table / Seats 10</td>
		</tr>
		<tr valign="top">
		  <td align="center"><img src="i/tokatee/kitchen_01.jpg" alt="Tokatee Kitchen" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/tokatee/kitchen_02.jpg" alt="Tokatee Kitchen" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Nice Kitchen / Stainless Appliances</td>
          <td align="center">Granite Counters</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/tokatee/bathroom_01.jpg" alt="Tokatee BathRoom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/tokatee/exterior_02.jpg" alt="Tokatee Exterior" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">Master Bath - Double Sinks</td>
		  <td align="center">Exterior - Double Car Garage</td>
	  </tr>
		
		<tr valign="top">
		  <td align="center"><img src="i/tokatee/hottub_01.jpg" alt="Tokatee Hot Tub" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/tokatee/deck_01.jpg" alt="Tokatee Big Deck" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Private Hot Tub</td>
		  <td align="center">Large Deck</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/tokatee/bedroom_02.jpg" alt="Tokatee Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/tokatee/bedroom_04.jpg" alt="Tokatee Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Master Bedroom / King Bed / Private Bath</td>
		  <td align="center">Bedroom #2 - Queen Bed</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/tokatee/bedroom_03.jpg" alt="Tokatee Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/tokatee/bedroom_01.jpg" alt="Tokatee Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Bedroom #3 - Queen Bed</td>
		  <td align="center">Bedroom #4 - Murphy Bed</td>
	  </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="tokatee.php">Back to Tokatee Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>

</body>
</html>
