<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cascade Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Cascade Lodge Video &amp; Photo Gallery</strong></h2>
<table width="425" cellpadding="3" cellspacing="0" border="0">
		<tr>
       	  <td colspan="2"><object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.trafficgeyser.net/getvideosecure/wf3qhU7f18c02481fb223ad901c606f278a182b-otj/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":true}' /></object></td>
        </tr>
		<tr>
			<td align="center" width="206"><img src="i/cascade/living_room_0307.jpg" alt="Sun River Fox Kitchen" width="250" height="188" border="1" /></td>
			<td align="center" width="206"><img src="i/cascade/living_room_above_0307.jpg" alt="Sun River Fox Living Room" width="250" height="188" border="1" /></td>
		</tr>
<tr valign="top">
			<td align="center">Living Room / Fireplace / 42&quot; Plasma TV</td>
			<td align="center">Living Room from Above</td>
		</tr>
		<tr valign="top">
		  <td align="center"><img src="i/cascade/kitchenCIMG0171.jpg" alt="Sun River Fox Kitchen" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/kitchen_SGB17.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Gourmet Kitchen / Granite Counters</td>
          <td align="center">Kitchen / Double Ovens / Stainless Appliances</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/cascade/dining_CIMG0166.jpg" alt="Sun River Fox Dining Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/2009_12_SecondDining.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
          <td align="center">Main Dining Room </td>
		  <td align="center">Second Dining Room </td>
	  </tr>
		
		<tr valign="top">
		  <td align="center"><img src="i/cascade/bedroom_CIMG0077.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/bedroom4_SGB17.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">King Master Suite #1</td>
		  <td align="center"> King Master Suite #2</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/cascade/bathroom_CIMG0071.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/bathroom_CIMG0072.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Luxurious Soaker Tub</td>
		  <td align="center">One of 5 Large Bathrooms</td>
	  </tr>
		<tr valign="top">
          <td align="center"><img src="i/cascade/bedroom_CIMG0066.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/bedroom_CIMG0089.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">King Bedroom</td>
		  <td align="center"> Queen Bedroom</td>
	  </tr>
		
		<tr valign="top">
          <td align="center"><img src="i/cascade/2009_12_BilliardsTable.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/gameroom2_0307.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Professional Billiards  Table</td>
		  <td align="center"> Bonus Room / TV / Video Games</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/cascade/2009_12_BunkRoom.jpg" alt="Sun River Fox Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/hottub_CIMG0110.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Bunk Room</td>
		  <td align="center">Private Hot Tub</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/cascade/2009_12_EntryWay.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/cascade/deck_CIMG0106.jpg" alt="Sun River Fox Spa" width="250" height="188" border="1" /></td>
    </tr>
		<tr valign="top">
		  <td align="center">Entry Hallway</td>
		  <td align="center">Back Deck / Large Stainless Bar-B-Q</td>
    </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="fox.php">Back to Cascade Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>

</body>
</html>
