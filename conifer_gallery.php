<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Conifer Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Conifer Lodge  Video &amp; Photo Gallery </strong></h2>
	<table width="425" cellpadding="3" cellspacing="0" border="0">
    	<tr>
          <td colspan="2"><object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/x_YSZc7f18c02481fb223ad901c606f278a182QOA76/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":true}' /></object></td>
        </tr>
		<tr>
		  <td align="center"><img src="i/conifer/front_new.jpg" alt="Sun River Conifer Entry Way" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/entry1_web.jpg" alt="Sun River Conifer Entry Way" width="250" height="188" border="1" /></td>
	  </tr>
		<tr>
		  <td align="center">Front Exterior</td>
		  <td align="center">Entry Way / Vaulted Ceilings</td>
	  </tr>
		<tr>
		  <td align="center"><img src="i/conifer/great_room.jpg" alt="Sun River Conifer Living Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/great_room02.jpg" alt="Sun River Conifer Living Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr>
		  <td align="center">Great Room / Big Screen TV / Fireplace</td>
		  <td align="center">Great Room / Comfortable Furniture</td>
	  </tr>
		<tr>
			<td align="center" width="206"><img src="i/conifer/kitchen1_web.jpg" alt="Sun River Conifer Kitchen" width="250" height="188" border="1" /></td>
			<td align="center" width="206"><img src="i/conifer/dining1_web.jpg" alt="Sun River Conifer Dining Room" width="250" height="188" border="1" /></td>
		</tr>
		<tr valign="top">
			<td align="center">Gourmet Kitchen with Pantry</td>
			<td align="center">Custom Log Table - Seats 14</td>
		</tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/bedroom1_web.jpg" alt="Sun River Conifer Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/bedroom2_web.jpg" alt="Sun River Conifer Master Suite" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Master Suite #1 - King Bed</td>
          <td align="center">Master Suite #1 - Fireplace</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/0726_bedroom_2.jpg" alt="Sun River Conifer Living Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/bathroom4_web.jpg" alt="Sun River Conifer Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">King Master Suite #2</td>
		  <td align="center">King Master Suite #2 - Bath</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/0726_bedroom_3.jpg" alt="Sun River Conifer Entry Way" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/0726_bedroom_1.jpg" alt="Sun River Conifer Entry Way" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Queen Master Suite / Private Bath</td>
		  <td align="center">King Bedroom / View Windows</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/tub1_web.jpg" alt="Sun River Conifer Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/0726_hallway.jpg" alt="Sun River Conifer Living Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Luxurious Soaker Tub</td>
		  <td align="center">Beautiful Log Accents Throughout</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/twin1_web.jpg" alt="Sun River Conifer Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/2009_12_BunkRoom.jpg" alt="Bonus Game Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Twin Bedroom / Walk In Closet</td>
		  <td align="center">Bunk Room / Sleeps 4 / TV/DVD</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/2009_12_BonusRoom.jpg" alt="Bonus Game Room" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/ChessTable_01_0208.jpg" alt="Bonus Game Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Bonus Room / Twin Bunks / TV/DVD / <br />
Video Games</td>
		  <td align="center">Custom Game Table</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/conifer/2009_12_HotTub.jpg" alt="Sun River Conifer Spa" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/conifer/2009_12_Garage.jpg" alt="Sun River Conifer Spa" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Bonus Room / Twin Bunks / TV/DVD / <br />
	      Video Games</td>
		  <td align="center">Ping Pong Table / 8 Bikes</td>
	  </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="conifer.php">Back to Conifer Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
