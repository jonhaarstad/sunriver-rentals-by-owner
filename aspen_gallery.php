<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Aspen Meadow Lodge Photo Gallery</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Aspen Meadow  Lodge Photo Gallery </strong></h2>
	<table width="425" cellpadding="3" cellspacing="0" border="0">
      <tr>
      	<td colspan="2"><object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/qNgWVo7f18c02481fb223ad901c606f278a1826AbXU/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":true}' /></object></td>
      </tr>
		<tr>
			<td align="center" width="206"><img src="i/aspen/KitchenB.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
		  <td align="center" width="206"><img src="i/aspen/Kitchen_121207_02.jpg" alt="Kitchen" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Gourmet Kitchen / Granite Counters</td>
		  <td align="center">Kitchen / Dining</td>
		</tr>
		<tr>
			<td align="center" width="206"><img src="i/aspen/Kitchen_Living_01.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
		  <td align="center" width="206"><img src="i/aspen/Living_121207_01.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Great Room / Comfortable Furniture</td>
			<td align="center">Great Room / Huge View Windows</td>
	  </tr>
		<tr>
			<td align="center" width="206"><img src="i/aspen/Living_121207_03.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
		  <td align="center" width="206"><img src="i/aspen/Living_TV_121207_02.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Great Room - Rock Fireplace</td>
			<td align="center">Great Room - 42&quot; Plasma TV</td>
	  </tr>
		<tr>
			<td align="center" width="206"><img src="i/aspen/Living_TV_121207_01.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
		  <td align="center" width="206"><img src="i/aspen/MasterBedroom0208.jpg" alt="Great Room" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
			<td align="center">Great Room - Lodge Decor</td>
		  <td align="center">King Master Suite #1</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/aspen/Bathtub_121207.jpg" alt="Master Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/aspen/MasterBathB.jpg" alt="Master Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
          <td align="center">Soaker Tub</td>
          <td align="center">Master Suite Bath</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/aspen/MasterBedroomDownstairs.jpg" alt="Master Bedroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/aspen/TileShower.jpg" alt="Master Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center"> King Master Suite #2</td>
		  <td align="center">Master Suite Shower</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/aspen/BunkBedsB.jpg" alt="Master Bathroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/aspen/Exterior.jpg" alt="Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Bunk Room / TV / Video Games</td>
		  <td align="center">Craftsman Exterior</td>
	  </tr>
		<tr valign="top">
		  <td align="center"><img src="i/aspen/2009_12_HotTub.jpg" alt="Master Bathroom" width="250" height="188" border="1" /></td>
		  <td align="center"><img src="i/aspen/2009_12_Deck.jpg" alt="Bedroom" width="250" height="188" border="1" /></td>
	  </tr>
		<tr valign="top">
		  <td align="center">Private Hot tub</td>
		  <td align="center">Private deck / Gas Grill</td>
	  </tr>
  	</table>
  <p><strong><br />
    &raquo;</strong> <a href="aspen.php">Back to Aspen Meadow Lodge Details Page </a></p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
