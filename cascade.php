<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cascade Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
	<h3>Sleeps 18:</h3>
	<ul>
  	  <li><strong>Bedroom #1</strong><br />
      King Bed (Master Suite)</li>
	  	<li> <strong>Bedroom #2</strong><br />
  	    King Bed (Master Suite)</li>
	  	<li><strong> Bedroom #3</strong> -
  	    King Bed </li>
	  	<li> <strong>Bedroom #4</strong> - Queen Bed</li>
	  	<li><strong>Bedroom #5</strong> - Queen Bed</li>
	  	<li><strong> Bedroom #6 </strong>- 4 Bunk Beds </li>
	  	<li><strong> Bedroom #7</strong> - Queen Sleeper-Sofa</li>
  	    <li><strong>Extra Bed</strong> - Twin Roll-a-Way</li>
	</ul>
	<h3>Special Features:</h3>
	<ul>
	  	<li>Air Conditioning</li>
	    <li>Hot Tub</li>
        <li>Free Wireless High-Speed Internet </li>
	  	<li>14 SHARC &amp; Tennis Passes</li>
	    <li>Pool Table</li>
	    <li>Ping Pong</li>
      <li>42&quot; Plasma TV in Living Room</li>
	    <li>Big Screen TV in Game Room</li>
	    <li>Lots of Family DVD Movies</li>
      <li>Sony Playstation 2 with Video Games</li>
	    <li>Board Games</li>
	    <li>Books &amp; Magazines</li>
	    <li>Two Dining Areas (seats up to 18)</li>
	    <li>River Rock Fireplace in Great Room</li>
	    <li>Beautiful Log Accents Throughout</li>
	    <li>2 Master Suites (with private baths)</li>
	    <li>TV in Every Room</li>
	    <li>8 Bikes</li>
	    <li>Large Lot </li>
	    <li>Plenty of parking</li>
	    <li>3-Car Garage</li>
	    <li>Great Location Near North End</li>
	    <li>Stereo CD System / CDs</li>
	    <li>Soaking Tub In Master Suite</li>
	    <li>Custom Log Accents Throughout</li>
	    <li>Gourmet Kitchen with Island Bar</li>
      <li>Solid Granite Counter Tops in Kitchen &amp; Baths</li>
	    <li>All Stainless Steel Appliances</li>
	    <li>Large Refrigerator</li>
	    <li>Double Ovens</li>
	    <li>Microwave</li>
	    <li>Washer &amp; Dryer</li>
	    <li>Large Natural Gas Bar-B-Q Grill</li>
	    <li>All Plush Linens Provided</li>
	    <li>All Cooking Utensils &amp; Dishes Provided </li>
	</ul>
  </div>
	<h2><strong>Cascade Lodge</strong> </h2>
	<p><img src="i/cascade/exterior_SGB17.jpg" alt="Sun River Rentals Fox Outside Front" width="250" height="188" class="imgLeft" /><strong>WELCOME TO CASCADE LODGE!</strong> Come relax in this premium five star vacation home! This luxurious 3500-square foot custom lodge-style home is the perfect place for your next Sunriver get-a-way. It features log accents throughout, beautiful interior design and furnishings, spectacular kitchen with solid granite counter tops, island bar, custom hickory cabinets, double oven, all stainless appliances, Hot Tub, Big Screen TV &amp; Pool Table in Game Room, 6 Bikes, and so much more! </p>
	<p>This magnificent home includes 7 bedrooms, including two Master Suites, and 5 spacious bathrooms. Great Room has a gorgeous river rock fireplace and comfortable seating for your entire group to relax. There are two dining areas with seating for up to 18. This house was designed with large groups in mind. Great for family reunions, wedding parties, couple groups, professional retreats, golf groups, conferences, church groups or multiple family vacations. </p>
	<p>Located at #17 Shagbark Lane this home is convenient to all Sunriver North amenities including Woodland's Golf Course, North Swimming Pool, Tennis Courts, Grocery store and more!</p>
	<p><strong><img src="i/cascade/living_room_0307.jpg" alt="Sun River Rentals Fox LIving Room" width="250" height="188" class="imgRight" /></strong>We also own Conifer Lodge (7 Bedrooms / 5 Baths)....Less than 5 minutes away.</p>
	<p><strong> No Smoking / No Pets</strong></p>
	<p><!-- &raquo; <a href="specials.php">See SPECIALS!</a><br /> -->
	  &raquo; <a href="cascade_gallery.php">Video &amp; Photo Gallery</a><br />
	  &raquo; <a href="http://www.vrbo.com/24376#calendar" target="_blank"> Availability Calendar </a><br />
      &raquo; <a href="http://www.vrbo.com/24376#rates" target="_blank">Rates</a><br />
	  &raquo; <a href="reservations.php">Make a Reservation</a></p>
	<p><strong>Activities (on site or nearby):</strong> 2 Swimming Pools, 31 Tennis Courts, 30 Miles of Paved Bike / Walking Paths, 2 Championship Golf Courses, Sage Springs Day Spa, Horseback Riding, Hiking, White Water Rafting, Rock Climbing, Biking, Golf, Racquetball, Basketball, Fitness Center, Horseshoes, Miniature Golf, Fishing, Hunting, Wildlife Viewing, Shopping, Restaurants, Museums, Sightseeing, Swimming, Boating, Water-skiing, Jet Skiing, Canoeing, Downhill Skiing, Cross Country Skiing, Snowboarding, Ice Skating, Snowmobiling, Sledding, ATVs, Mopeds.</p>
<!--
	<h3><strong>Cascade Lodge Rental Rates </strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$595 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$625 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$750 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$850 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$225</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 14 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
  <p>&nbsp;</p>
    <p><strong>Note:</strong> Until confirmed, rates are subject to change without notice.</p>
    <object width="480" height="385" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.trafficgeyser.net/getvideosecure/wf3qhU7f18c02481fb223ad901c606f278a182b-otj/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":false}' /></object>
    <?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
