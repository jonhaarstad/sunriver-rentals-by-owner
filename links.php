<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sun River Rentals by Owner :: Sunriver Links</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Links</strong></h2>
	<h3>Recreation</h3>
  <p>&raquo; <a href="http://www.mtbachelor.com/">Mt. Bachelor</a></strong><br />
    &raquo; <a href="http://www.golflink.com/golf-courses/city.aspx?dest=bend+or">Golf</a><br />
    &raquo; <a href="http://www.riverdrifters.net/">River Drifters (Whitewater Rafting)</a><br />
    &raquo; <a href="http://www.suncountrytours.com/">Sun Country Tours (Whitewater Rafting)</a><br />
    &raquo; <a href="http://www.coadventures.biz/">Sunriver Country Adventures</a><br />
    &raquo; <a href="http://rafting.allaboutrivers.com/Oregon_river_rafting/Oregon_white_water-stOR.html" title="AllAboutRivers Oregon Rafting Guide">AllAboutRivers Oregon Rafting Guide</a><br />
  &raquo; <a href="http://www.asrk.com">All Star  Rafting and Kayaking (Oregon &amp; Washington River Rafting)</a></p>
  <h3>General Interest</h3>
	<p><strong>&raquo; <a href="http://www.visitbend.org/">Bend Oregon Visitors Guide/Site </a><br />
    &raquo; <a href="http://www.highdesertmuseum.org/">High Desert Museum</a> <br />
&raquo; <a href="http://www.sunriverchamber.com/">Sunriver Chamber of Commerce</a><br />
&raquo; <a href="http://www.sunrivermusic.org/">Sunriver Music Festival </a><br />
  &raquo; </strong><strong><a href="http://www.sunrivernaturecenter.org/">Sunriver Nature Center</a> <br />
  &raquo; <a href="http://www.srh.noaa.gov/data/forecasts/ORZ043.php?warncounty=ORC017&city=Sunriver">Sunriver Weather </a></strong></p>
  <h3>Dining</h3>
	<p><strong>    &raquo; <a href="/dining_links.php">Sunriver Dining &amp; Food </a></strong></p>
	<p> <strong>The Meadows at the Lodge :: </strong> The menus feature ingredients indigenous to the Northwest, and an imaginative children's menu is available for all meals. Enjoy breathtaking views overlooking the Meadows Golf Course and Mt. Bachelor. The wine list features an extensive selection of Northwest vintages. 
Open for breakfast, lunch and dinner. The Meadows has received 
the Best menu design and also the prestigious Sante award. <strong>To 
make reservations, please call (541) 593-3740. </strong></p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
