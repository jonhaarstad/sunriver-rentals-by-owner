<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals by Owner :: Specials</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">

#special_section table {
	background: transparent;
}
#special_section td {
	background: inherit;
	border: none;
}
#special_section h2 {
	border-bottom: none;
}
#special_section table .month_temp {
	background: transparent;
	margin-bottom: 20px;
	border-right: 1px solid #cccccc;
}
#special_section table .month {
	font-family: sans-serif;
	text-align: center;
	background: #485427;
	padding: 5px;
	color: #ffffff;
}
#special_section table .day {
	font-family: sans-serif;
	font-size: 1.0em;
	padding: 5px;
	color: #ffffff;
	background: #777777;
}
#special_section table .date {
	font-family: sans-serif;
	font-size: 1.0em;
	background-color: #f9f9f9;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 3px;
	text-align: center;
}
#special_section table .key {
	font-family: sans-serif;
	font-size: 1.0em;
}
#special_section table .key td {
	border: 1px solid #c9c9c9;
	background-color: #f9f9f9;
}
#special_section table .key td h2 {
	font-size: 1.2em;
	margin:0;
	padding:7px 0 0 0;
	color: #400513;
}
#special_section table .key td p {
	margin: 0;
	padding: 0;
	font-size: 1.0em;
	margin-bottom: 0;
}
#special_section table .key td p strong {
	color: red;
}
#special_section table .discountRate {
	background-color: #5985B7;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	clear:both;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
}
#special_section table p {
	clear:left;
}
#special_section table .standardstandard {
	background-color: #FFAD00;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
}
#special_section table .standardThanks {
	background-color: #884E00;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
}
#special_section table .standardprime {
	background-color: #a82f25;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
	color: #ffffff;
}
#special_section table .peakNewYears {
	background-color: green;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
	color: #ffffff;
}
#special_section table .holidayWeekend {
	background-color: yellow;
	border: 1px solid #cccccc;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
	color: #000000;
}
#special_section table .oregondiscountBreak {
	background-color: #000;
	border: 1px solid #cccccc;
	color: #ffffff;
	display: block;
	width:80px;
	height:40px;
	float: left;
	margin-right: 10px;
	margin-bottom: 0;
	padding: 10px 5px 0 5px;
	vertical-align: middle;
	text-align: center;
}


#special_section table .discount {
	background-color: #5985B7;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
}
#special_section table .standard {
	background-color: #FFAD00;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
}
#special_section table .thanksgiving {
	background-color: #884E00;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
}
#special_section table .prime {
	background-color: #a82f25;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
	color: #ffffff;
}
#special_section table .newyears {
	background-color: green;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
	color: #ffffff;
}
#special_section table .holiday {
	background-color: yellow;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
	color: #000000;
}
#special_section table .discountbreak {
	background-color: #000;
	font-family: sans-serif;
	font-size: 1.0em;
	border-left: 1px solid #cccccc;
	border-bottom: 1px solid #cccccc;
	padding: 5px;
	text-align: center;
	color: #ffffff;
}

</style>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<!--<p><strong><img src="i/PROMO_wintershot_0208.jpg" alt="Winter in Sunriver, Oregon" width="225" height="169" class="imgRight" />The current specials on my properties include the following</strong>:</p> -->

<div id="special_section">
<table width="300" cellpadding="5" cellspacing="0" border="0" class="key" style="position:relative;left:-7px;">
	<tr>
		<td width="50%" valign="middle"><div class="discountRate">Discount<br /> Rate</div></td>
		<td><div class="standardstandard">Standard<br /> Rate</div></td>
		<td><div class="standardprime">Prime<br /> Rate</div></td>
		<td><div class="peakNewYears">Peak<br /> Rate</div></td>
	<!--<tr>
		<td width="50%"><div class="holidayWeekend">Holiday<br /> Rate</div></td>
		<td><div class="oregondiscountBreak">Spring<br /> Break</div></td>
	</tr>
	<tr>
		<td valign="top" style="whitespace:nowrap;"><div class="standardThanks">Thanksgiving<br /> Week</div></td>
		<td>&nbsp;</td>
	</tr>-->
</table>
	<h2><strong>Specials</strong></h2>
		<p style="font-size:.9em;"><!--<img src="i/but_discount.gif" />--><strong>*DISCOUNT RATE - 3rd Night Free</strong><br />
		<!--<img src="i/but_standard.gif" />--><strong>*STANDARD RATE - 3rd night Free</strong><br />
		<!--<img src="i/but_standard.gif" /><strong>*PECIAL - 4th Night Free</strong><br />-->
		<!--<img src="i/but_prime.gif" />--><strong>*SCHOOL HOLIDAYS - 4th Night Free</strong><br />
		&nbsp;&nbsp;&nbsp;(MLK Day / Presidents Day / Memorial Day / Spring Break)<br />
		<!--<img src="i/but_prime.gif" />--><strong>*THANKSGIVING WEEK - 5th Night Free</strong></p>

<h1>2013 Calendar</h1>
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
	<tr>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">January 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="newyears">1</td>
					<td class="standard">2</td>
					<td class="standard">3</td>
					<td class="standard">4</td>
					<td class="standard">5</td>
				</tr>
				<tr>
					<td class="standard">6</td>
					<td class="standard">7</td>
					<td class="standard">8</td>
					<td class="standard">9</td>
					<td class="standard">10</td>
					<td class="standard">11</td>
					<td class="standard">12</td>
				</tr>
				<tr>
					<td class="standard">13</td>
					<td class="standard">14</td>
					<td class="standard">15</td>
					<td class="standard">16</td>
					<td class="standard">17</td>
					<td class="prime">18</td>
					<td class="prime">19</td>
				</tr>
				<tr>
					<td class="prime">20</td>
					<td class="standard">21</td>
					<td class="standard">22</td>
					<td class="standard">23</td>
					<td class="standard">24</td>
					<td class="standard">25</td>
					<td class="standard">26</td>
				</tr>
				<tr>
					<td class="standard">27</td>
					<td class="standard">28</td>
					<td class="standard">29</td>
					<td class="standard">30</td>
					<td class="standard">31</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">February 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="standard">1</td>
					<td class="standard">2</td>
				</tr>
				<tr>
					<td class="standard">3</td>
					<td class="standard">4</td>
					<td class="standard">5</td>
					<td class="standard">6</td>
					<td class="standard">7</td>
					<td class="standard">8</td>
					<td class="standard">9</td>
				</tr>
				<tr>
					<td class="standard">10</td>
					<td class="standard">11</td>
					<td class="standard">12</td>
					<td class="standard">13</td>
					<td class="standard">14</td>
					<td class="prime">15</td>
					<td class="prime">16</td>
				</tr>
				<tr>
					<td class="prime">17</td>
					<td class="standard">18</td>
					<td class="standard">19</td>
					<td class="standard">20</td>
					<td class="standard">21</td>
					<td class="standard">22</td>
					<td class="standard">23</td>
				</tr>
				<tr>
					<td class="standard">24</td>
					<td class="standard">25</td>
					<td class="standard">26</td>
					<td class="standard">27</td>
					<td class="standard">28</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">March 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="standard">1</td>
					<td class="standard">2</td>
				</tr>
				<tr>
					<td class="standard">3</td>
					<td class="standard">4</td>
					<td class="standard">5</td>
					<td class="standard">6</td>
					<td class="standard">7</td>
					<td class="standard">8</td>
					<td class="standard">9</td>
				</tr>
				<tr>
					<td class="standard">10</td>
					<td class="standard">11</td>
					<td class="standard">12</td>
					<td class="standard">13</td>
					<td class="standard">14</td>
					<td class="standard">15</td>
					<td class="standard">16</td>
				</tr>
				<tr>
					<td class="standard">17</td>
					<td class="standard">18</td>
					<td class="standard">19</td>
					<td class="standard">20</td>
					<td class="standard">21</td>
					<td class="prime">22</td>
					<td class="prime">23</td>
				</tr>
				<tr>
					<td class="prime">24</td>
					<td class="prime">25</td>
					<td class="prime">26</td>
					<td class="prime">27</td>
					<td class="prime">28</td>
					<td class="prime">29</td>
					<td class="prime">30</td>
				</tr>
				<tr>
					<td class="prime">31</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">April 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="discount">1</td>
					<td class="discount">2</td>
					<td class="discount">3</td>
					<td class="discount">4</td>
					<td class="discount">5</td>
					<td class="discount">6</td>
				</tr>
				<tr>
					<td class="discount">7</td>
					<td class="discount">8</td>
					<td class="discount">9</td>
					<td class="discount">10</td>
					<td class="discount">11</td>
					<td class="discount">12</td>
					<td class="discount">13</td>
				</tr>
				<tr>
					<td class="discount">14</td>
					<td class="discount">15</td>
					<td class="discount">16</td>
					<td class="discount">17</td>
					<td class="discount">18</td>
					<td class="discount">19</td>
					<td class="discount">20</td>
				</tr>
				<tr>
					<td class="discount">21</td>
					<td class="discount">22</td>
					<td class="discount">23</td>
					<td class="discount">24</td>
					<td class="discount">25</td>
					<td class="discount">26</td>
					<td class="discount">27</td>
				</tr>
				<tr>
					<td class="discount">28</td>
					<td class="discount">29</td>
					<td class="discount">30</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">May 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="discount">1</td>
					<td class="discount">2</td>
					<td class="discount">3</td>
					<td class="discount">4</td>
				</tr>
				<tr>
					<td class="discount">5</td>
					<td class="discount">6</td>
					<td class="discount">7</td>
					<td class="discount">8</td>
					<td class="discount">9</td>
					<td class="discount">10</td>
					<td class="discount">11</td>
				</tr>
				<tr>
					<td class="discount">12</td>
					<td class="discount">13</td>
					<td class="discount">14</td>
					<td class="discount">15</td>
					<td class="discount">16</td>
					<td class="discount">17</td>
					<td class="discount">18</td>
				</tr>
				<tr>
					<td class="discount">19</td>
					<td class="discount">20</td>
					<td class="discount">21</td>
					<td class="discount">22</td>
					<td class="discount">23</td>
					<td class="prime">24</td>
					<td class="prime">25</td>
				</tr>
				<tr>
					<td class="prime">26</td>
					<td class="prime">27</td>
					<td class="standard">28</td>
					<td class="standard">29</td>
					<td class="standard">30</td>
					<td class="standard">31</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">June 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="standard">1</td>
				</tr>
				<tr>
					<td class="standard">2</td>
					<td class="standard">3</td>
					<td class="standard">4</td>
					<td class="standard">5</td>
					<td class="standard">6</td>
					<td class="standard">7</td>
					<td class="standard">8</td>
				</tr>
				<tr>
					<td class="standard">9</td>
					<td class="standard">10</td>
					<td class="standard">11</td>
					<td class="standard">12</td>
					<td class="standard">13</td>
					<td class="standard">14</td>
					<td class="standard">15</td>
				</tr>
				<tr>
					<td class="standard">16</td>
					<td class="standard">17</td>
					<td class="standard">18</td>
					<td class="standard">19</td>
					<td class="prime">20</td>
					<td class="prime">21</td>
					<td class="prime">22</td>
				</tr>
				<tr>
					<td class="prime">23</td>
					<td class="prime">24</td>
					<td class="prime">25</td>
					<td class="prime">26</td>
					<td class="prime">27</td>
					<td class="prime">28</td>
					<td class="prime">29</td>
				</tr>
				<tr>
					<td class="prime">30</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">July 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="prime">1</td>
					<td class="prime">2</td>
					<td class="prime">3</td>
					<td class="prime">4</td>
					<td class="prime">5</td>
					<td class="prime">6</td>
				</tr>
				<tr>
					<td class="prime">7</td>
					<td class="prime">8</td>
					<td class="prime">9</td>
					<td class="prime">10</td>
					<td class="prime">11</td>
					<td class="prime">12</td>
					<td class="prime">13</td>
				</tr>
				<tr>
					<td class="prime">14</td>
					<td class="prime">15</td>
					<td class="prime">16</td>
					<td class="prime">17</td>
					<td class="prime">18</td>
					<td class="prime">19</td>
					<td class="prime">20</td>
				</tr>
				<tr>
					<td class="prime">21</td>
					<td class="prime">22</td>
					<td class="prime">23</td>
					<td class="prime">24</td>
					<td class="prime">25</td>
					<td class="prime">26</td>
					<td class="prime">27</td>
				</tr>
				<tr>
					<td class="prime">28</td>
					<td class="prime">29</td>
					<td class="prime">30</td>
					<td class="prime">31</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">August 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="prime">1</td>
					<td class="prime">2</td>
					<td class="prime">3</td>
				</tr>
				<tr>
					<td class="prime">4</td>
					<td class="prime">5</td>
					<td class="prime">6</td>
					<td class="prime">7</td>
					<td class="prime">8</td>
					<td class="prime">9</td>
					<td class="prime">10</td>
				</tr>
				<tr>
					<td class="prime">11</td>
					<td class="prime">12</td>
					<td class="prime">13</td>
					<td class="prime">14</td>
					<td class="prime">15</td>
					<td class="prime">16</td>
					<td class="prime">17</td>
				</tr>
				<tr>
					<td class="prime">18</td>
					<td class="prime">19</td>
					<td class="prime">20</td>
					<td class="prime">21</td>
					<td class="prime">22</td>
					<td class="prime">23</td>
					<td class="prime">24</td>
				</tr>
				<tr>
					<td class="prime">25</td>
					<td class="prime">26</td>
					<td class="prime">27</td>
					<td class="prime">28</td>
					<td class="prime">29</td>
					<td class="prime">30</td>
					<td class="prime">31</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">September 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="prime">1</td>
					<td class="prime">2</td>
					<td class="standard">3</td>
					<td class="standard">4</td>
					<td class="standard">5</td>
					<td class="standard">6</td>
					<td class="standard">7</td>
				</tr>
				<tr>
					<td class="standard">8</td>
					<td class="standard">9</td>
					<td class="standard">10</td>
					<td class="standard">11</td>
					<td class="standard">12</td>
					<td class="standard">13</td>
					<td class="standard">14</td>
				</tr>
				<tr>
					<td class="standard">15</td>
					<td class="standard">16</td>
					<td class="standard">17</td>
					<td class="standard">18</td>
					<td class="standard">19</td>
					<td class="standard">20</td>
					<td class="standard">21</td>
				</tr>
				<tr>
					<td class="standard">22</td>
					<td class="standard">23</td>
					<td class="standard">24</td>
					<td class="standard">25</td>
					<td class="standard">26</td>
					<td class="standard">27</td>
					<td class="standard">28</td>
				</tr>
				<tr>
					<td class="standard">29</td>
					<td class="standard">30</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">October 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="discount">1</td>
					<td class="discount">2</td>
					<td class="discount">3</td>
					<td class="discount">4</td>
					<td class="discount">5</td>
				</tr>
				<tr>
					<td class="discount">6</td>
					<td class="discount">7</td>
					<td class="discount">8</td>
					<td class="discount">9</td>
					<td class="discount">10</td>
					<td class="discount">11</td>
					<td class="discount">12</td>
				</tr>
				<tr>
					<td class="discount">13</td>
					<td class="discount">14</td>
					<td class="discount">15</td>
					<td class="discount">16</td>
					<td class="discount">17</td>
					<td class="discount">18</td>
					<td class="discount">19</td>
				</tr>
				<tr>
					<td class="discount">20</td>
					<td class="discount">21</td>
					<td class="discount">22</td>
					<td class="discount">23</td>
					<td class="discount">24</td>
					<td class="discount">25</td>
					<td class="discount">26</td>
				</tr>
				<tr>
					<td class="discount">27</td>
					<td class="discount">28</td>
					<td class="discount">29</td>
					<td class="discount">30</td>
					<td class="discount">31</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">November 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="discount">1</td>
					<td class="discount">2</td>
				</tr>
				<tr>
					<td class="discount">3</td>
					<td class="discount">4</td>
					<td class="discount">5</td>
					<td class="discount">6</td>
					<td class="discount">7</td>
					<td class="discount">8</td>
					<td class="discount">9</td>
				</tr>
				<tr>
					<td class="discount">10</td>
					<td class="discount">11</td>
					<td class="discount">12</td>
					<td class="discount">13</td>
					<td class="discount">14</td>
					<td class="discount">15</td>
					<td class="discount">16</td>
				</tr>
				<tr>
					<td class="discount">17</td>
					<td class="discount">18</td>
					<td class="discount">19</td>
					<td class="discount">20</td>
					<td class="discount">21</td>
					<td class="discount">22</td>
					<td class="discount">23</td>
				</tr>
				<tr>
					<td class="prime">24</td>
					<td class="prime">25</td>
					<td class="prime">26</td>
					<td class="prime">27</td>
					<td class="prime">28</td>
					<td class="prime">29</td>
					<td class="prime">30</td>
				</tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table cellpadding="8" cellspacing="0" border="0" class="month_temp">
				<tr>
					<td colspan="7" class="month">December 2013</td>
				</tr>
				<tr>
					<td class="day">Su</td>
					<td class="day">Mo</td>
					<td class="day">Tu</td>
					<td class="day">We</td>
					<td class="day">Th</td>
					<td class="day">Fr</td>
					<td class="day">Sa</td>
				</tr>
				<tr>
					<td class="discount">1</td>
					<td class="discount">2</td>
					<td class="discount">3</td>
					<td class="discount">4</td>
					<td class="discount">5</td>
					<td class="discount">6</td>
					<td class="discount">7</td>
				</tr>
				<tr>
					<td class="discount">8</td>
					<td class="discount">9</td>
					<td class="discount">10</td>
					<td class="discount">11</td>
					<td class="discount">12</td>
					<td class="discount">13</td>
					<td class="discount">14</td>
				</tr>
				<tr>
					<td class="discount">15</td>
					<td class="prime">16</td>
					<td class="prime">17</td>
					<td class="prime">18</td>
					<td class="prime">19</td>
					<td class="prime">20</td>
					<td class="prime">21</td>
				</tr>
				<tr>
					<td class="prime">22</td>
					<td class="prime">23</td>
					<td class="newyears">24</td>
					<td class="newyears">25</td>
					<td class="newyears">26</td>
					<td class="newyears">27</td>
					<td class="newyears">28</td>
				</tr>
				<tr>
					<td class="newyears">29</td>
					<td class="newyears">30</td>
					<td class="newyears">31</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
					<td class="date">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<h1>Other Benefits Include:</h1>
<p><strong>* Free Wifi<br />
   * Free Movies<br />
   * Free Netflix Streaming<br />
   * Free Video Games<br />
   * Free SHARC and Tennis Passes (beginning May 25)</strong></p>
<p>&nbsp;</p>
</div>
  <?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
