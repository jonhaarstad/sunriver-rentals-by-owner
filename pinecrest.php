<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Pinecrest Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
    <h3>Rooms (sleeps 10):</h3>
    <ul>
      	<li> Master Bedroom - King Bed</li>
      	<li>Bedroom Two - King Bed</li>
      	<li>Bedroom Three - Two Twins</li>
      	<li>Large Loft - Two Twins</li>
      	<li>Living Room - Queen Hide-a-Bed</li>
    </ul>
    <h3>Special Features:</h3>
    <ul>
		<li>Great Location - Across From Village Mall</li>
		<li>Air Conditioning</li>
		<li>Free High Speed Wireless Internet</li>
		<li>Private Hot Tub</li>
	  	<li>8 SHARC &amp; Tennis Passes</li>
		<li>40&quot; Plasma TV in Living Room</li>
		<li>4TVs / DVD / 4VCRs</li>
	  	<li>Stereo CD System / CDs</li>
     	<li>50 Family Movies</li>
      	<li>Video Games - Nintendo 64</li>
      	<li>6 Bikes</li>
      	<li>Snow Tubes</li>
      	<li>Fuly Euipped Kitchen</li>
      	<li>Microwave</li>
   	  	<li>Refrigerator</li>
       	<li>Dishwasher</li>
       	<li>Washer and Dryer</li>
      	<li>Large Deck</li>
      	<li>Gas Barbecue</li>
       	<li>Large Private Lot</li>
       	<li>Toy Box Full of Toys</li>
      	<li>Family Board Games</li>
		<li>Books</li>
		<li>All Linens Provided</li>
 	</ul>
  </div>
	<h2><strong>Pinecrest Lodge </strong> </h2>
	<p><strong><img src="i/backwoods1/outside1_web.jpg" width="200" height="133" class="imgLeft" />LOCATION, LOCATION, LOCATION! You will love the location of this home. We are just a block from the Sunriver Village Mall, and three blocks from the spectacular Sunriver Lodge. Close to all Sunriver activities!</strong></p>
	<p><strong>We have recently remodeled</strong> this 1850-square-foot, lodge-style home, with newly upgraded furnishings, carpet, appliances, and hot tub. Loaded with lots of nice extras, this home includes three bedrooms, two baths, large loft, beautiful open floor plan with lots of natural light, vaulted ceiling with 17-foot rock fireplace, large master bedroom with private bath. This home has just received a 4 STAR RATING for its high quality and extra features.</p>
	<p><strong><img src="i/backwoods1/pinecrest_interior_0214.jpg" width="200" height="150" class="imgRight" />No Smoking / No Pets</strong></p>
<p><!-- &raquo; <a href="http://www.vrbo.com/153892#rates">See SPECIALS!</a><br /> -->
	  &raquo; <a href="pinecrest_gallery.php">Photo Gallery</a>	  <br />
	  &raquo;  <a href="http://www.vrbo.com/153892#calendar" target="_blank">Availability Calendar</a><br />
      &raquo; <a href="http://www.vrbo.com/153892#rates" target="_blank">Rates</a><br />
  	  &raquo; <a href="reservations.php">Make a Reservation</a></p>	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
<!--
	<h3><strong>Pinecrest Lodge  Rental Rates </strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$195 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$225 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$295 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$395 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$125</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 8 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
    <p>&nbsp;</p>
    <object width="480" height="385" border="3" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/FkBuZY7f18c02481fb223ad901c606f278a182pWeGW/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":false}' /></object>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
