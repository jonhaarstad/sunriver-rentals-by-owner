<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals by Owner :: Availability</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Availability</strong></h2>
	<p><strong>To check availability, please consult the following</strong> (use links provided):</p>
	<h3><strong>Conifer Lodge </strong></h3>
	<p><strong>&raquo;</strong> <a href="http://www.vrbo.com/34625#calendar">Check Availability (click here)</a></p>
	<h3><strong>Cascade Lodge </strong></h3>
	<p><strong>&raquo;</strong> <a href="http://www.vrbo.com/24376#calendar">Check Availability (click here)</a></p>
	<h3><strong>Evergreen Lodge </strong></h3>
	<p><strong>&raquo;</strong> <a href="http://www.vrbo.com/49784#calendar">Check Availability (click here) </a></p>
	<h3><strong>Pinecrest Lodge </strong></h3>
	<p><strong>&raquo;</strong> <a href="http://www.vrbo.com/153892#calendar">Check Availability (click here)</a></p>
	<p>&nbsp;</p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
