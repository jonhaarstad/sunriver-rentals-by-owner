<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sunriver Rentals by Owner :: Rates</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong> Sunriver  Nightly Rental Rates</strong></h2>
  <p><strong><img src="i/PROMO_wintershot002_0208.jpg" alt="Winter in Sunriver Oregon" width="225" height="169" class="imgRight" />For Seasonal SPECIALS, <a href="specials.php">click here</a> </strong></p>
  <h3><strong>Conifer Lodge </strong>:: Sleeps 20</h3>
  <p>&raquo; <a href="http://www.vrbo.com/34625#rates">View Rental Rates</a></p>
<!--
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$595 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$625 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$750 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$850 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$225</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 16 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
  <h3><strong>Cascade Lodge </strong>:: Sleeps 18</h3>
  <p>&raquo; <a href="http://www.vrbo.com/24376#rates">View Rental Rates</a></p>
<!--
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$595 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$625 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$750 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$850 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$225</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 14 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
  <h3><strong>Evergreen Lodge </strong>:: Sleeps 14</h3>
  <p>&raquo; <a href="http://www.vrbo.com/49784#rates">View Rental Rates</a></p>
<!--
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$350 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$375 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$475 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$575 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$175</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 12 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
<!--
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$275 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$295 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$350 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$450 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$165</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 10 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
  <h3><strong>Pinecrest Lodge  </strong>:: Sleeps 10 </h3>
  <p>&raquo; <a href="http://www.vrbo.com/153892#rates">View Rental Rates</a></p>
<!--
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$195 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$225 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$295 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$395 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$125</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 8 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
<!--
  <h3><strong>Tokatee Lodge  </strong>:: Sleeps 8</h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$175 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$195 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$250 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$325 / night - <a href="specials.php">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$125</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 8 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
  <p>&nbsp;</p>
    <h3><strong>DISCOUNT RATE SPECIAL</strong></h3>
    <p style="font-size:0.9em;"><strong>&raquo; 3rd Night FREE!</strong></p>
    <h3><strong>STANDARD RATE SPECIAL</strong></h3>
    <p style="font-size:0.9em;"><strong>&raquo; 3rd Night FREE!</strong> (except Spring Break)</p>
    <h3><strong>HOLIDAY WEEKEND SPECIAL</strong></h3>
    <p style="font-size:0.9em;"><strong>&raquo; 4th Night FREE!</p>
    <h3><strong>THANKSGIVING WEEK SPECIAL</strong></h3>
    <p style="font-size:0.9em;"><strong>&raquo; 5th Night FREE!</strong></p>

  <p>&nbsp;</p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
