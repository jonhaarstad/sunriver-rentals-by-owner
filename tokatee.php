<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Tokatee Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
<style type="text/css">
object {
	border: 4px solid #9E4218;
}
</style>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
	<h3>Sleeps 8:</h3>
	<ul>
  	  <li><strong>Bedroom #1</strong><br />
     	King Bed / TV / Private Bath</li>
	  	<li> <strong>Bedroom #2</strong><br />
  	    Queen Bed / TV</li>
	  	<li><strong> Bedroom #3</strong><br />
  	    Queen Bed / TV</li>
	  	<li> <strong>Bedroom #4</strong><br />
	  	Wall Bed / TV</li>
	</ul>
	<h3>Special Features:</h3>
	<ul>
		<li>Air Conditioning</li>
	  	<li>Free High-Speed Wireless Internet</li>
	  	<li>Free Movies</li>
	  	<li>8 SHARC &amp; Tennis Passes</li>
	  	<li>17' Stone Fireplace</li>
	  	<li>Private Hot Tub</li>
	  	<li>Double Sinks in Master Bath</li>
	  	<li>New 42" Plasma TV in Great Room</li>
	  	<li>DVD Player</li>
	  	<li>TV's in Every Room</li>
	  	<li>6 Bikes</li>
	  	<li>Fully Equipped Kitchen</li>
	  	<li>Stainless Appliances</li>
	  	<li>Microwave</li>
	  	<li>Refrigerator</li>
	  	<li>Dishwasher</li>
	  	<li>Washer and Dryer</li>
	  	<li>Large Deck</li>
	  	<li>Quiet Location</li>
	  	<li>Gas Bar-B-Q</li>
	  	<li>Board Games</li>
	  	<li>Books</li>
	  	<li>All Linens Provided</li>
	  	<li>No Smoking</li>
	  	<li>No Pets</li>
	</ul>
  </div>
	<h2><strong>Tokatee Lodge</strong> </h2>
	<p><img src="i/tokatee/exterior_02.jpg" alt="Sun River Rentals Tokatee Outside Front" width="250" height="188" class="imgLeft" /><strong>WELCOME TO TOKATEE LODGE!</strong> Tokatee Lodge is the perfect place for your next family vacation. This beautiful 4 Bedroom, 2 Bath single-level home features: Large Great Room, Vaulted Wood Ceilings, Stone Fireplace, Modern Kitchen with Stainless Appliances and Granite Counters, Large Dining Table (seats up to 10 people), New 42" Plasma TV, Private Hot Tub, Free Wi Fi, Free Netflix (streaming) and Free SHARC Passes.</p>
	<p>Located at #27 Tokatee Lane in Sunriver, it is just a short walk to the Woodlands Golf Course, Tennis Courts, North Pool, and Grocery Store.</p>
	<p><strong><img src="i/tokatee/greatroom_02.jpg" alt="Sun River Rentals Tokatee Living Room" width="250" height="188" class="imgRight" /></strong>We also own Conifer Lodge (7 Bedrooms / 5 Baths)....Less than 5 minutes away.</p>
	<p><strong> No Smoking / No Pets</strong></p>
	<p>&raquo; <a href="specials.php">See SPECIALS!</a><br />
	  &raquo; <a href="tokatee_gallery.php">Photo Gallery</a><br />
	  &raquo; <a href="http://www.vrbo.com/408184#calendar" target="_blank">Availability Calendar </a><br />
      &raquo; <a href="rates.php">Rates</a><br />
	  &raquo; <a href="reservations.php">Make a Reservation</a></p>
	<p><strong>Activities (on site or nearby):</strong> 2 Swimming Pools, 31 Tennis Courts, 30 Miles of Paved Bike / Walking Paths, 2 Championship Golf Courses, Sage Springs Day Spa, Horseback Riding, Hiking, White Water Rafting, Rock Climbing, Biking, Golf, Racquetball, Basketball, Fitness Center, Horseshoes, Miniature Golf, Fishing, Hunting, Wildlife Viewing, Shopping, Restaurants, Museums, Sightseeing, Swimming, Boating, Water-skiing, Jet Skiing, Canoeing, Downhill Skiing, Cross Country Skiing, Snowboarding, Ice Skating, Snowmobiling, Sledding, ATVs, Mopeds.</p>
	<h3><strong>Cascade Lodge Rental Rates </strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$175 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$195 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$250 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$325 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$125</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 8 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
  <p>&nbsp;</p>
    <p><strong>Note:</strong> Until confirmed, rates are subject to change without notice.</p>
    <?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
