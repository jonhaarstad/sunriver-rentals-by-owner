<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sun River Information</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Sunriver Information </strong></h2>
	<h3>A Message From The Sunriver Owners Association...</h3>
	<p><strong>Welcome To Sunriver!</strong> We hope you enjoy your stay in our neighborhood. We
	  have a few rules we observe for everyone's safety and consideration, and we'd
	  like you to observe them too.....</p>
	<p> <strong>+ Driving Speed:</strong>  Our speed limit maximum is 25 mph or
	  as posted.<br />
      <strong>+ Parking:</strong>  Because our streets are narrow, we must always
      provide room for emergency vehicles, so parking is allowed only in a home
      driveway or a designated parking area. Permission for special short-term
      parking can be obtained through the Sunriver Police Department (call 593-1014).<br />
  <strong>+ Bicycles:</strong>  Sunriver has over 30 miles of pathways. Get a
  map and go adventuring! However; please remember that bikes may be ridden only
  on established pathways and on the side lanes. Pedestrians and bikes should
  stay on the right except to pass. Pedestrians have the right of way on the
  paths, and cyclists should give an audible signal when passing. Please avoid
  use of golf course pathways.<br />
  <strong>+ Skating:</strong>  For safety reasons, roller blades, in-line skates,
  and ski-skates are not allowed. Our pathways and roads are not wide enough
  to safely accommodate them.<br />
  <strong>+ Pets</strong>:
  Please make sure your pets are on a leash or under voice control when outside.
  Help us all by cleaning up after your pet.<br />
  <strong>+ Hot tubs:</strong>  Respect your neighbors and the tranquility of Sunriver at all times while using
  your hot tub. The time to quit is 10 pm around here. Remember, sounds carry
  farther in our high desert air.<br />
  <strong>+ Golf Carts:</strong>  Stay to the right edge of the streets. Community pathways are for pedestrians
  and cyclists, not motorized vehicles.<br />
  <strong>+ RV's and Trailers:</strong>  Because recreation vehicles and boats are not allowed to be parked in driveways
  longer than 24 hours, space rental in the Sunriver RV parking lot may be obtained
  from the Sunriver Owners Association. Please contact Becki at 593-2411.<br />
  <strong>+ Littering:</strong>  Please help us keep Sunriver litter-free. Cleanliness counts!<br />
  <strong>+ Fire is Our Greatest Danger:</strong>  Properly maintained gas or electric barbecues are permitted and should be placed
  on patios or decks away from trees or ground cover. Charcoal Briquettes Are
  Not Allowed! Smoking is not permitted along roads, pathways, trails, or common
  areas.<br />
  <strong>+
  Evacuation:</strong>  In case of emergency, sirens will sound and the Police and Citizen Patrol members
  will give directions for evacuation. Tune to TV Cable Channel 4 for messages
  or tune into radio station FM 100.7 KICE for up to the minute information.
  The phone number at KICE is 383-3300.</p>
	<p>...Thanks and we'll observe the rules when we visit your neighborhood! Have
	  Fun! </p>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
