<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Evergreen Lodge</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<div id="amenities">
	<h3>Sleeps 14:</h3>
	<ul>
	  	<li>Bedroom #1 - Master w/ King Bed &amp; Private Bath</li>
      	<li>Bedroom #2 - Master w/ King Bed &amp; Private Bath</li>
      	<li>Bedroom #3 - Queen Bed</li>
      	<li>Bedroom #4 - Two Twins</li>
      	<li>Bedroom #5 - Twin Bunks</li>
      	<li>Bonus Room   -  Queen Hide-a-Bed</li>
      	<li>Living Room - New Queen Hide-a-Bed</li>
  	</ul>
  	<h3>Special Features:</h3>
    <ul>
		<li>Two Beautiful Master Suites w/ Private Baths</li>
		<li>Free High Speed Wireless Internet</li>
	  	<li>12 SHARC &amp; Tennis Passes</li>
	  	<li>Air Conditioning </li>
	  	<li>Huge Big Screen TV</li>
		<li>Large Deck</li>
		<li>Private Hot Tub</li>
		<li>River Rock Fireplace in Great Room</li>
		<li>Beautiful Wood Accents Throughout</li>
		<li>TV in Every Room</li>
		<li>Bonus/Game Room</li>
		<li>Video Games - Nintendo Game Cube</li>
		<li>7 Bikes</li>
	  	<li>Large Lot</li>
		<li>Double Car Garage</li>
		<li>Great Location Near Village Mall</li>
		<li>7 TVs / 5 DVDs / 2 VCRs</li>
		<li>Stereo CD System / CDs</li>
		<li>Ping Pong Table </li>
		<li>40 Family Movies</li>
		<li>New Kitchen with Island Bar</li>
		<li>All Stainless Appliances</li>
		<li>Microwave</li>
		<li>Laundry Room</li>
		<li>New Stainless Gas Bar-B-Q Grill</li>
		<li>All Plush Linens Provided</li>
		<li>All Cooking Utensils &amp; Dishes Provided</li>
	  </ul>
  </div>
	<h2><strong>Evergreen Lodge </strong> </h2>
	<p><strong><img src="i/backwoods5/backwoods5_outside.jpg" width="200" height="150" class="imgLeft" />BEST LOCATION IN SUNRIVER! </strong>Evergreen Lodge  is located directly across from the Sunriver Village Mall and walking distance to the spectacular Sunriver Lodge. It is close to all Sunriver activities! </p>
	<p><strong>Evergreen Lodge  is an exceptional 2500 square foot custom Craftsman home. </strong>A $100,000 extreme make-over has just been completed on this spectacular home! It features <strong>Five Bedroom plus Bonus Room and Four Baths,</strong> All New Kitchen, New Master Suite with Private Bath, Air Conditioning, New Carpeting throughout, New Furnishings and Appliances, New Decks, Hot Tub, River Rock Fireplace, Vaulted Ceilings, Huge Big-Screen TV, 7 Bikes, and so much more! You'll love the Lodge-Style feel of this home. Furnished by Mountain Comfort in Bend, this is a premium, 5 STAR smoke-free vacation home! Great location for families with children.</p>
	<p><strong><img src="i/backwoods5/home_img2.jpg" width="200" height="150" class="imgRight" />We also own Pinecrest Lodge. </strong>For large groups, the combination of these two beautiful homes will accommodate up to 24 people. Great for family reunions, golf groups, ski groups, church groups, business retreats, and more!</p>
	<p><strong>No Smoking / No Pets</strong></p>
	<p><!-- &raquo; <a href="specials.php">See SPECIALS!</a><br /> -->
  	   &raquo; <a href="evergreen_gallery.php">Photo Gallery</a><br />
  	   &raquo; <a href="http://www.vrbo.com/49784#calendar" target="_blank"> Availability Calendar </a><br />
  	   &raquo; <a href="http://www.vrbo.com/49784#rates" target="_blank">Rates</a><br />
  	   &raquo; <a href="reservations.php">Make a Reservation</a></p>
<!--
	<h3><strong>Evergreen Lodge Rental Rates</strong></h3>
	<table border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Discount Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$350 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Standard Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$375 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Prime Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$475 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Peak Rate:</p></td>
        <td class="pricing" align="right"><p align="left">$575 / night - <a href="specials.php" target="_blank">Click to See Dates</a></p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Cleaning Fee:</p></td>
        <td class="pricing" align="right"><p align="left">$175</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">Room Tax:</p></td>
        <td class="pricing" align="right"><p align="left">9%</p></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="pricing"><p align="right">SR Rec. Fee:</p></td>
        <td class="pricing" align="right"><p align="left">5% / <span style="font-weight:normal;">Provides 12 "Unlimited Use" Passes to SHARC &amp; Tennis Courts</span></p></td>
      </tr>
    </table>
-->
    <p>&nbsp;</p>
    <object width="480" height="385" border="3" bgcolor="#111111" data="http://www.siteproweb.com/flash/videoplayer.swf" type="application/x-shockwave-flash"><param name="movie" value="http://www.trafficgeyser.net/flash/videoplayer.swf" /><param name="allowfullscreen" value="true" /><param name="scale" value="noscale" /><param name="bgcolor" value="#111111" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="flashvars" value='config={"embedded":true,"menuItems":[true,true,true,true,true,false,false],"baseURL":"http://www.trafficgeyser.net/flash","videoFile":"http://www.siteproweb.com/getvideosecure/pflt247f18c02481fb223ad901c606f278a182d5kMS/hi","initialScale":"scale","controlBarBackgroundColor":"0x333333","autoBuffering":false,"loop":false,"autoPlay":false}' /></object>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
