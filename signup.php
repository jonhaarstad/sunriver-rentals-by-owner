<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sun River Rentals by Owner :: Reservations</title>
<link href="s/s.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include("inc/top.inc"); ?>
<?php include("inc/nav.inc"); ?>
<div id="content">
	<h2><strong>Sign Up for Our Email Specials</strong></h2>
	<p>Periodically, we'll inform you of any additional seasonal specials we may be offering on our  vacation rental homes.</p>
    <div id="contactForms">
	<form enctype='multipart/form-data' method="post" action="process_signup.php">
      <table width="500px" cellpadding="0" cellspacing="1" border="0">
			<tr class="light">
			  <td align="right">*Name:</td>
			  <td><input name="name" type="text" id="name" size="20" maxlength="50" /></td>
			</tr>
			<tr>
			  <td align="right">*Email Address:</td>
			  <td><input name="email" type="text" id="email" size="20" maxlength="50" /></td>
			</tr>
			<tr class="light">
			  <td align="right">Have You Stayed in One of Homes Before?</td>
			  <td><select name="haveyou" id="haveyou">
			    <option selected="selected">(Choose One)</option>
			    <option value="Yes">Yes</option>
			    <option value="No">No</option>
			  </select></td>
			</tr>
			<tr>
              <td align="right">How Did You Find Us?</td>
			  <td><select name="howdid" id="howdid">
                  <option selected="selected">(Choose One)</option>
                  <option value="Google Search">Google Search</option>
                  <option value="VRBO.com">VRBO.com</option>
                  <option value="GreatRentals.com">GreatRentals.com</option>
                  <option value="CyberRentals.com">CyberRentals.com</option>
                  <option value="VacationRentals.com">VacationRentals.com</option>
                  <option value="VacationRentals411.com">VacationRentals411.com</option>
                  <option value="GotYourSpot.com">GotYourSpot.com</option>
                  <option value="A-1 Vacations">A-1 Vacations</option>
				  <option value="Home Away">Home Away</option>
                  <option value="Sunriver Scene">Sunriver Scene</option>
                  <option value="Friend or Family">Friend or Family</option>
                  <option value="Other">Other</option>
                              </select>              </td>
	    </tr>
			<tr class="light">
			  <td align="right">Comments or Questions:</td>
				<td><textarea name="comments" cols="20" rows="4" id="comments" height="4"></textarea></td>
			</tr>
			<tr>
			  <td colspan="2" align="right"><table width="auto" border="2" cellpadding="5px" cellspacing="0" bordercolor="#333333" bgcolor="#C7C8AA">
                <tr bgcolor="#D2903F">
                  <td colspan="2"><font color="#FFFFFF"><strong>IMAGE VERIFICATION</strong></font></td>
                </tr>
                <tr>
                  <td><img src="/captcha/captchac_code.php" id="captcha" /></td>
                  <td valign="top"><strong>Please enter the text <br />
                    from the image:</strong><br />
                    <input type="text" name="Turing" value="" maxlength="100" size="10" />
                    <br />
                    [ <a href="#" onclick=" document.getElementById('captcha').src = document.getElementById('captcha').src + '?' + (new Date()).getMilliseconds()">Refresh Image</a> ] <br />
                    [ <a href="/captcha/whatisturing.html" onclick="window.open('/captcha/whatisturing.html','_blank','width=400, height=300, left=' + (screen.width-450) + ', top=100');return false;">What's This?</a> ]</td>
                </tr>
              </table></td>
	    </tr>
			<tr>
			  <td valign="top">&nbsp;</td>
			  <td><input type="submit" name="Submit" value="Submit" />
		      <input type="reset" name="Submit2" value="Clear Form" /></td>
		  </tr>
	  </table>
  </form>
  </div>
	<?php include("inc/copy.inc"); ?>
</div>
</body>
</html>
